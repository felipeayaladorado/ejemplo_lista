﻿Public Class claseNodo
    Private NodoDato As Object
    Private NodoSiguiente As claseNodo

    Public Sub New(ByVal dato As Object, ByVal siguiente As claseNodo)
        NodoDato = dato
        NodoSiguiente = siguiente
    End Sub

    Function setDatos(ByVal dato As Object) As Object
        NodoDato = dato
        Return Nothing
    End Function

    Function getDatos() As Object
        Return NodoDato
    End Function

    Function setSiguiente(ByVal siguiente As claseNodo) As claseNodo
        NodoSiguiente = siguiente
        Return Nothing
    End Function

    Function getSiguiente() As claseNodo
        Return NodoSiguiente
    End Function

    'Se debe implementar métodos de búsqueda y eliminación
End Class
