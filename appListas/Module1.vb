﻿Module Module1

    Sub Main()
        Dim inicio As claseNodo
        Dim actual As claseNodo
        'Dim lectura As String = ""
        inicio = Nothing

        For contandor As Integer = 1 To 5
            'lectura = Console.ReadLine()
            If inicio Is Nothing Then
                inicio = New claseNodo(contandor, Nothing)
            Else
                inicio = New claseNodo(contandor, inicio)
            End If
        Next

        actual = inicio
        While actual IsNot Nothing
            Console.WriteLine(actual.getDatos().ToString)
            actual = actual.getSiguiente()
        End While
        Console.ReadKey()
    End Sub

End Module
